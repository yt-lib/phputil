<?php
namespace YtLib\PhpUtil;

class Path {
	private function __construct() {}

	static function join(string $p1, string $p2): string {
		if ($p2 = \ltrim($p2, '/')) {
			return \rtrim($p1, '/') . '/' . $p2;
		}
		return $p1;
	}

	static function joinAll(string $p1, string ...$p2): string {
		if (!$p2) return $p1;
		foreach ($p2 as $tmp) if ($tmp = \ltrim($tmp, '/')) $p1 = \rtrim($p1, '/') . '/' . $tmp;
		return $p1;
	}
}
