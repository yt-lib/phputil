<?php
namespace YtLib\PhpUtil;

class Parse {
	private function __construct() {}
	/**
	 * 文字列の配列を返す
	 *
	 * 引数が文字列ならば ',' で分割して返す
	 * 引数が配列ならば文字列のみに filter して返す
	 *
	 * @param string|string[] $any
	 * @return string[]
	 */
	static function asStrings($any): array {
		if (\is_string($any)) {
			return \preg_split('/,/', $any, 0, \PREG_SPLIT_NO_EMPTY);
		} elseif (\is_array($any)) {
			return \array_filter($any, function($k) {
				return \is_string($k);
			});
		}
		return [];
	}
}
