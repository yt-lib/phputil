<?php
namespace YtLib\PhpUtil;

final class Hook {
	/** @var array<string, list<callable>> */
	private $_hooks = [];

	/**
	 * @param callable $fn
	 * @return static
	 */
	function on(string $timing, $fn) {
		if (isset($this->_hooks[$timing]) and \is_array($this->_hooks[$timing])) {
		} else $this->_hooks[$timing] = [];
		$this->_hooks[$timing][] = $fn;
		return $this;
	}

	/**
	 * @param mixed $value
	 * @param mixed[] $other_args
	 * @return mixed
	 * @psalm-suppress MixedAssignment
	 */
	function _apply(string $timing, $value, ...$other_args) {
		if (isset($this->_hooks[$timing]) and \is_array($this->_hooks[$timing]))
			foreach ($this->_hooks[$timing] as $fn)
				$value = \call_user_func($fn, $value, ...$other_args);
		return $value;
	}
}
