<?php
namespace YtLib\PhpUtil\Traits;

trait GettableAttributeFromMethod {
	static private $_GettableAttributeFromMethod_fns = null;
	static function _GettableAttributeFromMethod_getFn($type, $name) {
		if (is_null(self::$_GettableAttributeFromMethod_fns)) {
			self::$_GettableAttributeFromMethod_fns = ['get' => [], 'set' => []];
			foreach (get_class_methods(self::class) as $fn)
				if (preg_match('/(get|set)([a-z0-9_]+)Attribute/mui', $fn, $matchs))
					self::$_GettableAttributeFromMethod_fns[strtolower($matchs[1])][strtolower($matchs[2])] = $fn;
		}
		return self::$_GettableAttributeFromMethod_fns[$type][strtolower($name)] ?? NULL;
	}
	function __unset($name) {
		unset($this->_body[$name]);
	}
	function __isset($name) {
		return isset($this->_body[$name]) or
			self::_GettableAttributeFromMethod_getFn('get', $name);
	}
	function __get($name) {
		\is_array($this->_body) or $this->_body = [];
		if (array_key_exists($name, $this->_body)) return $this->_body[$name];
		if ($fn = self::_GettableAttributeFromMethod_getFn('get', $name))
			return $this->_body[$name] = $this->$fn();
		return null;
	}
	function __set($name, $value) {
		if ($fn = self::_GettableAttributeFromMethod_getFn('set', $name))
			return $this->_body[$name] = $this->$fn($value);
		return $this->_body[$name] = $value;
	}
}
