<?php
namespace YtLib\PhpUtil;

class Is {
	private function __construct() {}

	/**
	 * @param mixed $v
	 * @return bool
	 * @psalm-assert-if-true list<mixed> $v
	 * @psalm-suppress MixedAssignment
	 */
	static function arraylist($v) {
		if (!\is_array($v)) return false;
		$i = 0;
		foreach ($v as $k => $t)
			if ($k !== $i++) return false;
		return true;
	}

	/**
	 * @param mixed $v
	 * @return bool
	 * @psalm-assert-if-true string[] $v
	 */
	static function strings($v) {
		return Is::any($v, 'is_string');
	}

	/**
	 * @param mixed $v
	 * @return bool
	 * @psalm-assert-if-true list<string> $v
	 */
	static function stringlist($v) {
		return Is::arraylist($v) and Is::strings($v);
	}

	/**
	 * @param mixed $v
	 * @param callable $fn
	 * @return bool
	 * @psalm-assert-if-true mixed[] $v
	 * @psalm-suppress MixedAssignment
	 */
	static function any($v, $fn) {
		if (!\is_array($v)) return false;
		foreach ($v as $t)
			if (!\call_user_func($fn, $t)) return false;
		return true;
	}

	/**
	 * @param mixed $v
	 * @param callable $fn
	 * @return bool
	 * @psalm-assert-if-true mixed[] $v
	 * @psalm-suppress MixedAssignment
	 */
	static function some($v, $fn) {
		if (!\is_array($v)) return false;
		foreach ($v as $t)
			if (\call_user_func($fn, $t)) return true;
		return false;
	}

	/**
	 * @param mixed $list
	 * @return bool
	 * @psalm-assert-if-true iterable<mixed> $list
	 */
	static function iterable($list) {
		return \is_array($list) or (
			\is_object($list) and
			($list instanceof \Traversable)
		);
	}

	/**
	 * @param mixed $value
	 * @return bool
	 * @psalm-assert-if-true string $value
	 * @psalm-suppress MixedArgument, MixedArrayAssignment, MixedArrayAccess
	 */
	static function validEmailAddress($value) {
		return \is_string($value)
			and $value
			and $p = \explode('@', $value)
			and 1 < \count($p)
			and $p[0] = \preg_replace('/\\.+/mui', '.', \trim($p[0], '.'))
			and \filter_var(\join('@', $p), \FILTER_VALIDATE_EMAIL);
	}
}
