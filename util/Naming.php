<?php
namespace YtLib\PhpUtil;

class Naming {
	private function __construct() {}

	public static function underscore(string $str): string {
		return \ltrim(\strtolower(\preg_replace('/[A-Z]/', '_\0', $str)), '_');
	}

	public static function hyphen(string $str): string {
		return \ltrim(\strtolower(\preg_replace('/[A-Z]/', '-\0', $str)), '-');
	}

	public static function camelize(string $str): string {
		return \lcfirst(\strtr(\ucwords(\strtr($str, ['_' => ' '])), [' ' => '']));
	}

	public static function phpHeader(string $str): string {
		return \strtoupper(\preg_replace('/-/', '_', 'http-' . $str));
	}
}
