<?php
namespace YtLib\PhpUtil;

use PHPUnit\Framework\TestCase;

class PathTest extends TestCase {
	function testJoin(): void {
		$this->assertEquals('hoge/piyo', Path::join('hoge', 'piyo'));
		$this->assertEquals('hoge/piyo/', Path::join('hoge', 'piyo/'));
		$this->assertEquals('/hoge/piyo', Path::join('/hoge', 'piyo'));
		$this->assertEquals('/hoge/piyo', Path::join('/hoge/', 'piyo'));
		$this->assertEquals('/hoge/piyo', Path::join('/hoge/', '/piyo'));
		$this->assertEquals('/hoge/piyo/fuga', Path::join('/hoge/', '/piyo/fuga'));
	}
	function testJoinAll(): void {
		$this->assertEquals('hoge', Path::joinAll('hoge'));
		$this->assertEquals('hoge/', Path::joinAll('hoge/'));
		$this->assertEquals('hoge/piyo', Path::joinAll('hoge', 'piyo'));
		$this->assertEquals('/hoge/piyo', Path::joinAll('/hoge', 'piyo'));
		$this->assertEquals('/hoge/piyo', Path::joinAll('/hoge/', 'piyo'));
		$this->assertEquals('/hoge/piyo', Path::joinAll('/hoge/', '/piyo'));
		$this->assertEquals('/hoge/piyo/fuga', Path::joinAll('/hoge/', '/piyo/fuga'));
		$this->assertEquals('hoge/piyo/fuga', Path::joinAll('hoge', 'piyo', 'fuga'));
		$this->assertEquals('1/2/3/4/5', Path::joinAll('1', '2/', '/3/', '4', '/5'));
	}
}
